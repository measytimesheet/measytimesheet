package com.coins.mobile.measytimesheet.test.controller;

import android.test.InstrumentationTestCase;
import android.util.Log;

import com.coins.mobile.measytimesheet.controller.CommunicationHelper;
import com.coins.mobile.webservices.interfaces.JcTimesheetI;

import java.util.Date;

/**
 * Created by Andrey.Zaburdaev on 17.06.2014.
 */
public class CommunicationHelperTest extends InstrumentationTestCase {
    private static final String TAG = "CommunicationHelperTest";

    public void testaddTimesheet() throws Exception {
        CommunicationHelper ch = CommunicationHelper.getInstance(null);

        ch.addTimesheet(
                new JcTimesheetI() {
                    @Override
                    public long getDateMilliseconds() {
                        return new Date().getTime();
                    }

                    @Override
                    public String getActivity() {
                        return "OHMEET";
                    }

                    @Override
                    public String getDescription() {
                        return "test record";
                    }

                    @Override
                    public String getAnalysis() {
                        return "PR010.7260.LA";
                    }

                    @Override
                    public Double getHours() {
                        return 2.5;
                    }
                }
        );

        Log.v(TAG, "testaddTimesheet " + "test");

    }
}
