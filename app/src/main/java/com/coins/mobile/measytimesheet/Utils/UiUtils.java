package com.coins.mobile.measytimesheet.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.coins.mobile.measytimesheet.activity.EditDialogFragment;
import com.coins.mobile.measytimesheet.activity.MainActivity;
import com.coins.mobile.measytimesheet.activity.MessageDialog;

public class UiUtils {
    public static <T extends View> T getView(Activity parent, int viewId) {
        return (T) checkView(parent.findViewById(viewId));
    }

    public static <T extends View> T getViewOrNull(Activity parent, int viewId) {
        return (T) parent.findViewById(viewId);
    }

    public static <T extends View> T getView(View parent, int viewId) {
        return (T) checkView(parent.findViewById(viewId));
    }

    public static <T extends View> T getViewOrNull(View parent, int viewId) {
        return (T) parent.findViewById(viewId);
    }

    private static View checkView(View v) {
        if (v == null) {
            throw new IllegalArgumentException("View doesn't exist");
        }
        return v;
    }

    public static void forceHideKeyboard(final Activity activity, final View view) {
        if (activity == null || view == null) {
            return;
        }
        final InputMethodManager inputManager = (InputMethodManager)
                activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void forceShowKeyboard(final Activity activity, final View view) {
        if (activity == null || view == null) {
            return;
        }
        final InputMethodManager inputManager = (InputMethodManager)
                activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(view, 0);
    }

    public static <T> void showEditDialog(Context context, EditDialogFragment.Callback<T> callback, int editType,
                                          T object, String value, String hint) {
        if (context != null && context instanceof MainActivity) {
            final MainActivity activity = (MainActivity) context;
            EditDialogFragment<T> editDialog = (EditDialogFragment<T>) activity.
                    getSupportFragmentManager().findFragmentByTag(EditDialogFragment.TAG);
            if (editDialog == null) {
                editDialog = new EditDialogFragment<T>();
                editDialog.setValues(callback, editType, object, value, hint);
                activity.getSupportFragmentManager().beginTransaction()
                        .add(editDialog, EditDialogFragment.TAG)
                        .commit();
            }
        } else {
            throw new IllegalArgumentException("incorrect context " + context);
        }
    }

    public static <T> void showConfirmMessageDialog(Context context, MessageDialog.Callback<T> callback, String message) {
        if (context != null && context instanceof MainActivity) {
            final MainActivity activity = (MainActivity) context;
            MessageDialog<T> errorDialog = (MessageDialog<T>) activity.
                    getSupportFragmentManager().findFragmentByTag(MessageDialog.TAG);
            if (errorDialog == null) {
                errorDialog = new MessageDialog<T>();
                errorDialog.setValues(callback, message);
                activity.getSupportFragmentManager().beginTransaction()
                        .add(errorDialog, MessageDialog.TAG)
                        .commit();
            }
        } else {
            throw new IllegalArgumentException("incorrect context " + context);
        }
    }
}
