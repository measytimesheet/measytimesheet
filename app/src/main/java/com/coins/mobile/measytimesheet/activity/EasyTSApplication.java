package com.coins.mobile.measytimesheet.activity;

import android.app.Application;

import com.coins.mobile.measytimesheet.models.Timesheet;

public class EasyTSApplication extends Application {
  private long startTime;
    private Timesheet mCurrentTimesheet;


    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStartTime() {

        return startTime;
    }

    public Timesheet getCurrentTimesheet() {
        return mCurrentTimesheet;
    }

    public void setCurrentTimesheet(Timesheet currentTimesheet) {
        this.mCurrentTimesheet = currentTimesheet;
    }
}
