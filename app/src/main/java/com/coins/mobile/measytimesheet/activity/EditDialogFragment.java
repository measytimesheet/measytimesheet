package com.coins.mobile.measytimesheet.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.coins.mobile.measytimesheet.R;
import com.coins.mobile.measytimesheet.Utils.UiUtils;

public class EditDialogFragment<T> extends com.actionbarsherlock.app.SherlockDialogFragment {

    public final static String TAG = EditDialogFragment.class.getSimpleName();
    public static final int EDIT_TEXT_DIALOG = 1;
    public static final int EDIT_NUMBER_DIALOG = 2;
    private int dialogType;
    private Callback<T> mCallback;
    private T mObject;
    private String mValue;
    private String mHint;
    private EditText mEdit;

    public void setValues(Callback<T> callback,
                          int type,
                          T object, String value, String hint) {
        dialogType = type;
        mCallback = callback;
        mObject = object;
        mValue = value;
        mHint = hint;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.edit_dialog, container, false);

        mEdit = (EditText) v.findViewById(R.id.edit_text);

        if (dialogType == EDIT_NUMBER_DIALOG) {
            mEdit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        } else if (dialogType == EDIT_TEXT_DIALOG) {
            v.setMinimumWidth(1000);
            mEdit.setInputType(InputType.TYPE_CLASS_TEXT);
            mEdit.setSingleLine(false);
            mEdit.setLines(3);
        }

        if (mValue == null || mValue.isEmpty()) {
            mEdit.setHint(mHint);
        } else {
            mEdit.setText(mValue);
        }
        mEdit.requestFocus();
        UiUtils.forceShowKeyboard(getActivity(), mEdit);

        final View ok = v.findViewById(R.id.dialog_ok);
        final View cancel = v.findViewById(R.id.dialog_cancel);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int viewId = v.getId();
                UiUtils.forceHideKeyboard(getActivity(), v);
                dismiss();
                if (viewId == R.id.dialog_ok) {
                    if (mCallback != null) {
                        String value = ((EditText) v.getRootView().findViewById(R.id.edit_text)).getText().toString();
                        mCallback.onConfirmObjectAction(mObject, value);
                    }
                } else if (viewId == R.id.dialog_cancel) {
                    if (mCallback != null) {
                        mCallback.onCancelObjectAction(mObject);
                    }
                }
            }
        };
        cancel.setOnClickListener(onClickListener);
        if (mCallback != null) {
            ok.setOnClickListener(onClickListener);
        } else {
            ok.setVisibility(View.GONE);
        }
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return v;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    public void onResume() {
        super.onResume();
        mEdit.requestFocus();

        mEdit.postDelayed(new Runnable() {

            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(mEdit, 0);
            }
        }, 200);
    }

    public interface Callback<T> {
        void onConfirmObjectAction(T object, String value);

        void onCancelObjectAction(T object);
    }
}