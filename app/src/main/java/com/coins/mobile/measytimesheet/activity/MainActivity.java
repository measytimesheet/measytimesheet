package com.coins.mobile.measytimesheet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.coins.mobile.measytimesheet.R;
import com.coins.mobile.measytimesheet.Utils.UiUtils;
import com.coins.mobile.measytimesheet.controller.CommunicationHelper;
import com.coins.mobile.measytimesheet.models.Timesheet;
import com.coins.mobile.webservices.WSFaultException;
import com.coins.mobile.webservices.interfaces.JcvJacValReturnI;

import java.io.IOException;

public class MainActivity extends SherlockFragmentActivity {

    private FragmentManager fragmentManager;
    private static Context appContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = getBaseContext();
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        if (((EasyTSApplication) getApplication()).getStartTime() > 0) {
            showTimerFragment();
        } else {
            showOhCardsListFragment();
        }
    }

    public void showTimerFragment() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragments, new TimerFragment(),TimerFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void showOhCardsListFragment() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragments, new OhCardsListFragment(),OhCardsListFragment.class.getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {

        if(fragmentManager.findFragmentByTag(TimerFragment.class.getSimpleName()) != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragmentManager.findFragmentByTag(TimerFragment.class.getSimpleName()));
            fragmentTransaction.commitAllowingStateLoss();

            showOhCardsListFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsActivity = new Intent(getBaseContext(),
                    Preferences.class);
            startActivity(settingsActivity);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public Timesheet getCurrentTimesheet() {
        return ((EasyTSApplication) getApplication()).getCurrentTimesheet();
    }

    public void setCurrentTimesheet(Timesheet currentTimesheet) {
        ((EasyTSApplication) getApplication()).setCurrentTimesheet(currentTimesheet);
    }

    public void proceedTimesheet(Timesheet currentTimesheet) {
        ((EasyTSApplication)getApplication()).setStartTime(0);
        setCurrentTimesheet(null);
        if(currentTimesheet != null) {
            submitTimesheet(currentTimesheet);
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(fragmentManager.findFragmentByTag(TimerFragment.class.getSimpleName()));
        fragmentTransaction.commitAllowingStateLoss();
        showOhCardsListFragment();
    }

    public void submitTimesheet(Timesheet currentTimesheet){
        addTimesheetAsyncTask addTimesh = new addTimesheetAsyncTask(appContext);
        addTimesh.execute(currentTimesheet);

    }

    private static class addTimesheetAsyncTask extends AsyncTask<Timesheet, Void, String> {
        private Context mContext;

        public addTimesheetAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected String doInBackground(Timesheet... params) {
            CommunicationHelper ch = CommunicationHelper.getInstance(mContext);
            try {
                JcvJacValReturnI activityDefs = ch.getTimsheetActivityDefaults(params[0].getActivity());
                Timesheet ts = new Timesheet(params[0].getId(), params[0].getKco(), params[0].getUserId(),
                        params[0].getDateMilliseconds(), params[0].getActivity(), params[0].getDescription().length() > 0?params[0].getDescription():activityDefs.getJac_desc(), activityDefs.getJac_entry(),
                        activityDefs.getJac_analysis(), params[0].getHours());
                ch.addTimesheet(ts);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (WSFaultException e) {
                e.printStackTrace();
                return e.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null && !result.isEmpty()) {
                Toast toast = Toast.makeText(mContext, result, Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }
}
