package com.coins.mobile.measytimesheet.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.coins.mobile.measytimesheet.R;
import com.coins.mobile.measytimesheet.Utils.UiUtils;

public class MessageDialog<T> extends com.actionbarsherlock.app.SherlockDialogFragment {

    public final static String TAG = MessageDialog.class.getSimpleName();

    private final static String PARAM_MESSAGE_STRING = "param_message_string";

    private String mErrorMessageString;
    private Callback<T> mCallback;

    public void setValues(Callback<T> callback,
                          String error) {
        mErrorMessageString = error;
        mCallback = callback;
    }

    public void setValues(String error) {
        mErrorMessageString = error;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.alert_dialog, container, false);

        final TextView textView = (TextView) root.findViewById(R.id.alert_message);
        if (savedInstanceState != null) {
            mErrorMessageString = savedInstanceState.getString(PARAM_MESSAGE_STRING);
        }
        textView.setText(mErrorMessageString);

        final View ok = root.findViewById(R.id.dialog_ok);
        final View cancel = root.findViewById(R.id.dialog_cancel);
        View.OnClickListener onClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int viewId = v.getId();
                dismiss();
                if (viewId == R.id.dialog_ok) {
                    if (mCallback != null) {
                        mCallback.onConfirmObjectAction();
                    }
                } else if(viewId == R.id.dialog_cancel) {
                    if (mCallback != null) {
                        mCallback.onCancelObjectAction();
                    }
                }
            }
            };
        cancel.setVisibility(View.GONE);
        final TextView okText = UiUtils.getView(root, R.id.dialog_ok_text);
        okText.setText(R.string.ok_label);

        if (mCallback != null) {
            ok.setOnClickListener(onClickListener);
        } else {
            ok.setVisibility(View.GONE);
        }
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(PARAM_MESSAGE_STRING, mErrorMessageString);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    public interface Callback<T> {
        void onConfirmObjectAction();

        void onCancelObjectAction();
    }
}