package com.coins.mobile.measytimesheet.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.coins.mobile.measytimesheet.R;
import com.coins.mobile.measytimesheet.Utils.UiUtils;
import com.coins.mobile.measytimesheet.models.Timesheet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OhCardsListFragment extends SherlockFragment implements AdapterView.OnItemClickListener {

    protected GridView mListView;
    private TsGridAdapter docsListItemAdapter;
    private ArrayList<Timesheet> timesheets;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.oh_cards_list_fragment, null);
        initCommonUI(root);
        return root;
    }

    protected void initCommonUI(View root) {
        mListView = UiUtils.getView(root, android.R.id.list);
        this.docsListItemAdapter = new TsGridAdapter((MainActivity)getActivity(), R.layout.ts_item, this.timesheets);
        this.mListView.setAdapter(docsListItemAdapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        timesheets = new ArrayList<Timesheet>();
        timesheets.add(new Timesheet(0,0,null,0, "OHMEET","Overheads Internal Meetings",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHLRNG","Overheads - Learning",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHDOWN","System Downtime/Recovery",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHADMN","Overhead - Administration",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHTIME","Timesheet Entry",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHINMT","Overheads - Int Cust Mtg",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHPERS","Overheads - Personal",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHMKUP","Overheads - Make up hours",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHMGMT","Overheads - Management",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHMAIL","Overheads - General e-mail etc",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHINTR","Intranet Updates",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHHOLS","Overheads - Holidays",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHEXMT","Overheads - Ext Cust Mtg",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHDOCU","Overheads - Documentation",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHBILL","Overheads - Billing",null,null,0.0));
        timesheets.add(new Timesheet(0,0,null,0, "OHACPL","Overheads-Account Planning",null,null,0.0));

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Timesheet timesheet = (Timesheet) view.getTag();
        ((MainActivity)getActivity()).setCurrentTimesheet(timesheet);
        UiUtils.showEditDialog(getActivity(), callback, EditDialogFragment.EDIT_TEXT_DIALOG, timesheet, "", timesheet.getDescription());
    }

    final EditDialogFragment.Callback callback = new EditDialogFragment.Callback() {
        @Override
        public void onConfirmObjectAction(Object object, String answerValue) {
            if(answerValue.length() > 0) {
                final Timesheet timesheet = (Timesheet)object;
                timesheet.setDescription(answerValue);
                ((MainActivity)getActivity()).setCurrentTimesheet(timesheet);
            }
            ((MainActivity)getActivity()).showTimerFragment();
        }

        @Override
        public void onCancelObjectAction(Object object) {
        }
    };

    public class TsGridAdapter extends ArrayAdapter<Timesheet> {

        protected Activity controller;
        protected int resourceId;
        protected List<Timesheet> list;

        public TsGridAdapter(MainActivity activity, int resourceId, ArrayList<Timesheet> timesheets) {
            super(activity, resourceId, timesheets);
            this.controller = activity;
            this.resourceId = resourceId;
            this.list = timesheets;
        }

        @Override
        public int getCount() {
            int result = 0;
            if(list != null) {
                result = list.size();
            }
            return result;
        }

        @Override
        public Timesheet getItem(int position) {
            return list.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(this.resourceId, null);
            }

            TextView tsCardCode = (TextView) v.findViewById(R.id.ts_card);
            if (position < this.list.size()) {
                Timesheet timesheet = this.list.get(position);
                tsCardCode.setText(timesheet.getActivity());
                v.setTag(timesheet);
            }
            return v;

        }
    }
}
