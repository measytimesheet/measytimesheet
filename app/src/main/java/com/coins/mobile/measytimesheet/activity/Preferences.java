package com.coins.mobile.measytimesheet.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.coins.mobile.measytimesheet.R;

public class Preferences extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}