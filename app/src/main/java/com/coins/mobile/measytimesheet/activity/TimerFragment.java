package com.coins.mobile.measytimesheet.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.coins.mobile.measytimesheet.R;
import com.coins.mobile.measytimesheet.Utils.UiUtils;
import com.coins.mobile.measytimesheet.models.Timesheet;

import java.util.Date;

public class TimerFragment extends SherlockFragment implements View.OnClickListener {

    private TextView mCode;
    private EditText mDesc;
    private TextView timerValue;
    private Button stopButton;
    private long startTime = 0L;

    private Handler customHandler = new Handler();

    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    @Override
    public void onResume() {
        super.onResume();
        startTime = ((EasyTSApplication)getActivity().getApplication()).getStartTime();
        if(startTime > 0){
            updatedTime =SystemClock.uptimeMillis() - startTime;
            customHandler.postDelayed(updateTimerThread, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.timer_fragment, null);
        initCommonUI(root);
        return root;
    }

    protected void initCommonUI(View root) {
        mCode = UiUtils.getView(root, R.id.activity_code);
        Timesheet timesheet = ((MainActivity)getActivity()).getCurrentTimesheet();
        mDesc = UiUtils.getView(root, R.id.activity_desc);
        timerValue = (TextView)  UiUtils.getView(root, R.id.timerValue);
        mCode.setText(timesheet.getActivity());
        mDesc.setHint(timesheet.getDescription());

        startTime = ((EasyTSApplication)getActivity().getApplication()).getStartTime();
        if(startTime == 0) {
            startTime = SystemClock.uptimeMillis();
        }
        timeSwapBuff = 0;
        customHandler.postDelayed(updateTimerThread, 0);
        ((EasyTSApplication)getActivity().getApplication()).setStartTime(startTime);

        stopButton = (Button)  UiUtils.getView(root,R.id.stopButton);

        stopButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                Double hours = Double.valueOf(Math.round( (double)( (double)updatedTime/(double)(15*60*1000) ) ) * 15) / 60;
                if(hours == 0 ) {
                    customHandler.removeCallbacks(updateTimerThread);
                    UiUtils.showConfirmMessageDialog(getActivity(), callback, getActivity().getString(R.string.time_less_than_25_message, null));
                } else {
                    Timesheet timesheet = ((MainActivity) getActivity()).getCurrentTimesheet();
                    customHandler.removeCallbacks(updateTimerThread);
                    ((MainActivity) getActivity()).proceedTimesheet(new Timesheet(0, 0, null, new Date().getTime(), timesheet.getActivity(), timesheet.getDescription(), null, null, hours));
                }
            }
        });
    }

    MessageDialog.Callback callback = new MessageDialog.Callback() {
        @Override
        public void onConfirmObjectAction() {
            ((MainActivity) getActivity()).proceedTimesheet(null);
        }

        @Override
        public void onCancelObjectAction() {

        }
    };

    @Override
    public void onPause() {
        super.onPause();
        customHandler.removeCallbacks(updateTimerThread);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onClick(View v) {

    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int seconds = (int) (updatedTime / 1000) % 60 ;
            int minutes = (int) ((updatedTime / (1000*60)) % 60);
            int hours   = (int) ((updatedTime / (1000*60*60)) % 24);
            int milliseconds = (int) (updatedTime % 1000);
            timerValue.setText("" + hours + ":"
                    + "" + minutes + ":"
                    + String.format("%02d", seconds) + ":"
                    + String.format("%03d", milliseconds));
            customHandler.postDelayed(this, 0);
        }

    };
}
