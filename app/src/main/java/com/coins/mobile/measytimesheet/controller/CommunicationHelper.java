package com.coins.mobile.measytimesheet.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.coins.mobile.measytimesheet.models.JcEmployee;
import com.coins.mobile.measytimesheet.models.User;
import com.coins.mobile.webservices.JcEmployeeWS;
import com.coins.mobile.webservices.JcTimesheetWS;
import com.coins.mobile.webservices.UserWS;
import com.coins.mobile.webservices.WSFaultException;
import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.JcEmployeeI;
import com.coins.mobile.webservices.interfaces.JcTimesheetI;
import com.coins.mobile.webservices.interfaces.JcvJacValReturnI;
import com.coins.mobile.webservices.interfaces.UserI;
import com.coins.mobile.webservices.jcvjacWS;

import java.io.IOException;
import java.util.List;

/**
 * Created by Andrey.Zaburdaev on 17.06.2014.
 */
public class CommunicationHelper {
    private static CommunicationHelper ourInstance;

    private static final JcTimesheetWS timesheetWS = JcTimesheetWS.getInstance();
    private static final JcEmployeeWS employeeWSws = JcEmployeeWS.getInstance();
    private static final UserWS userWS = UserWS.getInstance();
    private static final jcvjacWS jcvJacWS = jcvjacWS.getInstance();

    private final SharedPreferences preferences;

    public static synchronized CommunicationHelper getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new CommunicationHelper(context);
        }
        return ourInstance;
    }

    private CommunicationHelper(Context context) {
        preferences = PreferenceManager
                .getDefaultSharedPreferences(context);
    }

    public void addTimesheet(JcTimesheetI timesheet) throws IOException, WSFaultException {

        User user = new User(preferences.getString("userIdPref", ""), preferences.getString("userPasswordPref", ""), -1);
        final String envURL = preferences.getString("environmenturl", "http://oaenvs.coins-global.com/cgi-bin/live.cgi/wousoap.p");
        ConnectionDetailsI connDet = new ConnectionDetailsI() {
            @Override
            public String getEnvironmentUrl() {
                return envURL;
            }
        };

        List<UserI> users = userWS.getUser(User.UserFactory.getInstance(), connDet, user);

        user.setKco(users.get(0).getKco());
        List<JcEmployeeI> empls = employeeWSws.getEmployee(JcEmployee.JcEmployeeFactory.getInstance(), connDet, user);
        String employeeRowid = empls.get(0).getJemRowid();
        timesheetWS.add(employeeRowid, timesheet, connDet, user);

    }

    public JcvJacValReturnI getTimsheetActivityDefaults(String jac_code) throws IOException, WSFaultException {

        User user = new User(preferences.getString("userIdPref", ""), preferences.getString("userPasswordPref", ""), -1);
        final String envURL = preferences.getString("environmenturl", "http://oaenvs.coins-global.com/cgi-bin/live.cgi/wousoap.p");
        ConnectionDetailsI connDet = new ConnectionDetailsI() {
            @Override
            public String getEnvironmentUrl() {
                return envURL;
            }
        };

        List<UserI> users = userWS.getUser(User.UserFactory.getInstance(), connDet, user);

        user.setKco(users.get(0).getKco());
        jcvjacWS.ValidReturn valRet = jcvJacWS.validateCode(jac_code, user.getUserid(), connDet, user);
        return valRet;

    }

}
