package com.coins.mobile.measytimesheet.models;

import com.coins.mobile.webservices.interfaces.FactoryI;
import com.coins.mobile.webservices.interfaces.JcEmployeeI;

import java.util.Map;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public class JcEmployee implements JcEmployeeI {
    private final String jem_code;
    private final String jem_name;
    private final String jemRowid;

    public JcEmployee(String jem_code, String jemRowid, String jem_name) {
        this.jem_code = jem_code;
        this.jem_name = jem_name;
        this.jemRowid = jemRowid;
    }


    public String getJem_code() {
        return jem_code;
    }

    @Override
    public String getJem_name() {
        return jem_name;
    }

    @Override
    public String getJemRowid() {
        return jemRowid;
    }

    public static class JcEmployeeFactory implements FactoryI<JcEmployeeI> {
        private static JcEmployeeFactory ourInstance = new JcEmployeeFactory();

        public static JcEmployeeFactory getInstance() {
            return ourInstance;
        }

        private JcEmployeeFactory() {
        }

        @Override
        public JcEmployeeI create(Map<String, String> fields) {
            return new JcEmployee(fields.get("jem_code"), fields.get("jc_employeerowid"), fields.get("jem_name"));
        }
    }

}
