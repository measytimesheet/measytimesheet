package com.coins.mobile.measytimesheet.models;

import com.coins.mobile.webservices.interfaces.FactoryI;
import com.coins.mobile.webservices.interfaces.JcTimesheetI;

import java.util.Map;

public class Timesheet implements JcTimesheetI {
    public static final String TABLE_NAME = "jc_timesheet";

    public static final String _ID = "_id";
    public static final String KCO = "kco";
    public static final String USER_ID = "jem_code";
    public static final String DATE_TIME_MILLISECONDS = "jts_date";
    public static final String ACTIVITY = "jac_code";
    public static final String DESCRIPTION = "jts_desc";
    public static final String ENTRY = "jts_entry";
    public static final String ANALYSIS = "jts_analysis";
    public static final String HOURS = "jts_hours";

    private final long mId;
    private final int mKco;
    private final String mUserId;
    private final long mDateMilliseconds;
    private final String mActivity;
    private String mDescription;
    private final String mEntry;
    private final String mAnalysis;
    private final Double mHours;

    public Timesheet(long id, int kco, String userId, long date,
                     String activity, String description, String entry, String analysis, Double hours) {
        mId = id;
        mKco = kco;
        mUserId = userId;
        mDateMilliseconds = date;
        mActivity = activity;
        mDescription = description;
        mEntry = entry;
        mAnalysis = analysis;
        mHours = hours;

    }

    public long getId() {
        return mId;
    }

    public int getKco() {
        return mKco;
    }

    public String getUserId() {
        return mUserId;
    }

    public long getDateMilliseconds() {
        return mDateMilliseconds;
    }

    public String getActivity() {
        return mActivity;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String desc){
        mDescription = desc;
    }

    public String getEntry() {
        return mEntry;
    }

    public String getAnalysis() {
        return mAnalysis;
    }

    public Double getHours() {
        return mHours;
    }

    public static class JcTimesheetFactory implements FactoryI<JcTimesheetI> {
        private static JcTimesheetFactory ourInstance = new JcTimesheetFactory();

        public static JcTimesheetFactory getInstance() {
            return ourInstance;
        }

        private JcTimesheetFactory() {
        }

        @Override
        public JcTimesheetI create(Map<String, String> fields) {
            return new Timesheet(0, 0, "", Long.parseLong(fields.get("date")),
                    fields.get("activity"),
                    fields.get("description"), "",
                    fields.get("analysis"), Double.valueOf(fields.get("hours")));
        }
    }
}