package com.coins.mobile.measytimesheet.models;

import com.coins.mobile.webservices.interfaces.FactoryI;
import com.coins.mobile.webservices.interfaces.UserI;

import java.util.Map;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public class User implements UserI {
    private final String userid;
    private final String password;
    private int kco;

    public User(String userid, String password, int kco) {
        this.userid = userid;
        this.password = password;
        this.kco = kco;
    }

    public String getUserid() {
        return userid;
    }

    public String getPassword() {
        return password;
    }

    public void setKco(int kco) {
        this.kco = kco;
    }

    public int getKco() {
        return kco;
    }

    public static class UserFactory implements FactoryI<UserI> {
        private static UserFactory ourInstance = new UserFactory();

        public static UserFactory getInstance() {
            return ourInstance;
        }

        private UserFactory() {
        }

        @Override
        public UserI create(Map<String, String> fields) {
            return new User(fields.get("su-userid"), "", Integer.parseInt(fields.get("sur_primeCo")));
        }
    }
}
