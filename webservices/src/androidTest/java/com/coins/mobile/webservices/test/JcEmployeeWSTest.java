package com.coins.mobile.webservices.test;

import android.test.InstrumentationTestCase;
import android.util.Log;

import com.coins.mobile.webservices.JcEmployeeWS;
import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.FactoryI;
import com.coins.mobile.webservices.interfaces.JcEmployeeI;
import com.coins.mobile.webservices.interfaces.UserI;

import java.util.List;
import java.util.Map;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public class JcEmployeeWSTest extends InstrumentationTestCase {
    private static final String TAG = "JcEmployeeWSTest";

    public void testgetEmployee() throws Exception {

        JcEmployeeWS ws = JcEmployeeWS.getInstance();

        List<JcEmployeeI> ls = ws.getEmployee(new FactoryI<JcEmployeeI>() {
                                                  @Override
                                                  public JcEmployeeI create(final Map<String, String> fields) {
                                                      return new JcEmployeeI() {
                                                          @Override
                                                          public String getJem_name() {
                                                              return fields.get("jem_name");
                                                          }

                                                          @Override
                                                          public String getJemRowid() {
                                                              return fields.get("jc_employeerowid");
                                                          }
                                                      };
                                                  }
                                              },
                new ConnectionDetailsI() {
                    @Override
                    public String getEnvironmentUrl() {
                        return "http://oaenvs.coins-global.com/cgi-bin/live.cgi/wousoap.p";
                    }
                },
                new UserI() {
                    @Override
                    public String getUserid() {
                        return "andzab";
                    }

                    @Override
                    public String getPassword() {
                        return "andzab";
                    }

                    @Override
                    public int getKco() {
                        return 0;
                    }
                }
        );

        for (JcEmployeeI l : ls) {
            Log.v(TAG, "testgetEmployee JcEmployeeI " + l.getJem_name() + " " + l.getJemRowid());
        }

        final int expected = 1;
        final int reality = 1;
        assertEquals(expected, reality);
    }

}
