package com.coins.mobile.webservices.test;

import android.test.InstrumentationTestCase;

import com.coins.mobile.webservices.JcTimesheetWS;
import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.JcTimesheetI;
import com.coins.mobile.webservices.interfaces.UserI;

import java.util.Date;

/**
 * Created by Andrey.Zaburdaev on 17.06.2014.
 */
public class JcTimesheetWSTest extends InstrumentationTestCase {
    private static final String TAG = "JcTimesheetWSTest";

    public void tesadd() throws Exception {

        JcTimesheetWS ws = JcTimesheetWS.getInstance();

        ws.add("0x000000000031e90e",
                new JcTimesheetI() {
                    @Override
                    public long getDateMilliseconds() {
                        return new Date().getTime();
                    }

                    @Override
                    public String getActivity() {
                        return "OHMEET";
                    }

                    @Override
                    public String getDescription() {
                        return "test record";
                    }

                    @Override
                    public String getAnalysis() {
                        return "PR010.7260.LA";
                    }

                    @Override
                    public Double getHours() {
                        return 2.5;
                    }
                },
                new ConnectionDetailsI() {
                    @Override
                    public String getEnvironmentUrl() {
                        return "http://oaenvs.coins-global.com/cgi-bin/live.cgi/wousoap.p";
                    }
                },
                new UserI() {
                    @Override
                    public String getUserid() {
                        return "andzab";
                    }

                    @Override
                    public String getPassword() {
                        return "andzab";
                    }

                    @Override
                    public int getKco() {
                        return 10;
                    }
                }
        );


    }
}
