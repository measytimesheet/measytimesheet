package com.coins.mobile.webservices.test;

import android.test.InstrumentationTestCase;

import com.coins.mobile.webservices.buildxmlrequest.Call;
import com.coins.mobile.webservices.buildxmlrequest.XMLRequestHelper;
import com.coins.mobile.webservices.interfaces.UserI;

import java.util.List;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public class XMLRequestHelperTest extends InstrumentationTestCase {

    public void testRequestSOAPMessage1() throws Exception {

        XMLRequestHelper xmlRequestHelper = XMLRequestHelper.getInstance();

        List<Call> calls = null;

        xmlRequestHelper.RequestSOAPMessage(new UserI() {
            @Override
            public String getUserid() {
                return "andzab";
            }

            @Override
            public String getPassword() {
                return "andzab";
            }

            @Override
            public int getKco() {
                return 10;
            }
        },calls);

        assertEquals(1, 1);

    }

    public void testRequestSOAPMessage2() throws Exception {

        XMLRequestHelper xmlRequestHelper = XMLRequestHelper.getInstance();

        Call call = null;

        xmlRequestHelper.RequestSOAPMessage(new UserI() {
            @Override
            public String getUserid() {
                return "andzab";
            }

            @Override
            public String getPassword() {
                return "andzab";
            }

            @Override
            public int getKco() {
                return 10;
            }
        },call);

        assertEquals(1, 1);

    }
}
