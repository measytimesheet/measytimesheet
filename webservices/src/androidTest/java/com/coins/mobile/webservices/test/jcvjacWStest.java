package com.coins.mobile.webservices.test;

import android.test.InstrumentationTestCase;
import android.util.Log;

import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.UserI;
import com.coins.mobile.webservices.jcvjacWS;

/**
 * Created by Andrey.Zaburdaev on 18.06.2014.
 */
public class jcvjacWStest extends InstrumentationTestCase {
    private static final String TAG = "jcvjacWStest";

    public void testvalidateCode() throws Exception {
        jcvjacWS ws = jcvjacWS.getInstance();

        jcvjacWS.ValidReturn vr = ws.validateCode("OHADMNa", "andzab",
                new ConnectionDetailsI() {
                    @Override
                    public String getEnvironmentUrl() {
                        return "http://oaenvs.coins-global.com/cgi-bin/live.cgi/wousoap.p";
                    }
                },
                new UserI() {
                    @Override
                    public String getUserid() {
                        return "andzab";
                    }

                    @Override
                    public String getPassword() {
                        return "andzab";
                    }

                    @Override
                    public int getKco() {
                        return 10;
                    }
                }
        );
    }
}
