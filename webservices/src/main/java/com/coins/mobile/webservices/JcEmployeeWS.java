package com.coins.mobile.webservices;

import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.FactoryI;
import com.coins.mobile.webservices.interfaces.JcEmployeeI;
import com.coins.mobile.webservices.interfaces.UserI;

import java.io.IOException;
import java.util.List;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public class JcEmployeeWS extends WebService<JcEmployeeI> {
    private static final String TAG = "JcEmployeeWS";

    private static final String TLA = "jem";
    private static final String TABLE = "jc_employee";
    private static final String GET_EMPLOYEE_QUERY = "FOR EACH %s WHERE jc_employee.kco = %s AND jc_employee.jem_code = '%s'";
    private static final String GET_FIELDS = "jem_name,jem_code";

    private static JcEmployeeWS ourInstance = new JcEmployeeWS();

    public static JcEmployeeWS getInstance() {
        return ourInstance;
    }

    private JcEmployeeWS() {
    }

    public List<JcEmployeeI> getEmployee(FactoryI<JcEmployeeI> fact, ConnectionDetailsI conDet, UserI user)
            throws WSFaultException, IOException {
        return getRecords(fact, conDet, user, String.format(GET_EMPLOYEE_QUERY, TABLE, user.getKco(), user.getUserid()), TABLE, GET_FIELDS);
    }
}