package com.coins.mobile.webservices;

import com.coins.mobile.webservices.buildxmlrequest.Call;
import com.coins.mobile.webservices.buildxmlrequest.XMLRequestHelper;
import com.coins.mobile.webservices.buildxmlrequest.commitCurrent;
import com.coins.mobile.webservices.buildxmlrequest.getLastRowCreated;
import com.coins.mobile.webservices.buildxmlrequest.getRowFieldValue;
import com.coins.mobile.webservices.buildxmlrequest.setAddRowid;
import com.coins.mobile.webservices.buildxmlrequest.setBufferField;
import com.coins.mobile.webservices.buildxmlrequest.setVar;
import com.coins.mobile.webservices.buildxmlrequest.setjc_timesheetQuery;
import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.FactoryI;
import com.coins.mobile.webservices.interfaces.JcEmployeeI;
import com.coins.mobile.webservices.interfaces.JcTimesheetI;
import com.coins.mobile.webservices.interfaces.UserI;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public class JcTimesheetWS {
    private static final String tla = "jts";
    private static final String TAG = "JcTimesheetWS";
    final static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    private static JcTimesheetWS ourInstance = new JcTimesheetWS();

    public static JcTimesheetWS getInstance() {
        return ourInstance;
    }

    private JcTimesheetWS() {
    }

    public String add(String jc_employeeRowid, JcTimesheetI timesheet, ConnectionDetailsI conDet, UserI user) throws IOException, WSFaultException {
        List<Call> calls = new ArrayList<Call>();

        XMLRequestHelper<JcTimesheetI> xmlRequestHelper = XMLRequestHelper.getInstance();

        calls.add(new setVar("jc_employeeRowid", jc_employeeRowid));
        calls.add(new setAddRowid(tla + "-rsp"));
        calls.add(new setAddRowid(tla + "-rsp"));
        calls.add(new setBufferField(tla + "-rsp", "jts_date", 0, formatter.format(new Date(timesheet.getDateMilliseconds()))));
        calls.add(new setBufferField(tla + "-rsp", "jac_code", 0, timesheet.getActivity()));
        calls.add(new setBufferField(tla + "-rsp", "jts_desc", 0, timesheet.getDescription()));
        calls.add(new setBufferField(tla + "-rsp", "jts_hours", 0, timesheet.getHours().toString()));
        calls.add(new setBufferField(tla + "-rsp", "jts_analysis", 0, timesheet.getAnalysis()));
        calls.add(new commitCurrent(tla + "-rsp"));
        calls.add(new getLastRowCreated(tla + "-rsp"));
        calls.add(new setjc_timesheetQuery("FOR EACH jc_timesheet WHERE ROWID(jc_timesheet) = TO-ROWID(DYNAMIC-FUNCTION('getLastRowCreated')) "));
        calls.add(new getRowFieldValue(tla + "-rsp", "jts_lastrev", 0));

        String xml = xmlRequestHelper.RequestSOAPMessage(user, calls);

        XMLRequestHelper.Response resp = xmlRequestHelper.sendXMLRequest(conDet, xml);

        if (resp.getContentType().equalsIgnoreCase("text/xml")) {
            XMLRequestHelper.XMLResponse XMLresp = xmlRequestHelper.parseXMLResponse
                    (resp.getContent());

            if (XMLresp.getFault() != null)
                throw new WSFaultException(XMLresp.getFault().getReasonText());

        } else {
            throw new WSFaultException(resp.getContent());
        }

        return "";
    }

    public JcTimesheetI getTimesheet(FactoryI<JcTimesheetI> fact) {
        return fact.create(null);
    }
}
