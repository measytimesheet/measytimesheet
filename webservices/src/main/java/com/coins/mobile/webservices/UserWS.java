package com.coins.mobile.webservices;

import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.FactoryI;
import com.coins.mobile.webservices.interfaces.JcEmployeeI;
import com.coins.mobile.webservices.interfaces.UserI;

import java.io.IOException;
import java.util.List;

/**
 * Created by andrey on 18.06.2014.
 */
public class UserWS extends WebService<UserI> {
    private static final String TAG = "UserWS";

    private static final String TLA = "sur";
    private static final String TABLE = "sysuser";
    private static final String GET_USER_QUERY = "FOR EACH %s WHERE su-userid = '%s'";
    private static final String GET_FIELDS = "su-userid,sur_primeCo";

    private static UserWS ourInstance = new UserWS();

    public static UserWS getInstance() {
        return ourInstance;
    }

    private UserWS() {
    }

    public List<UserI> getUser(FactoryI<UserI> fact, ConnectionDetailsI conDet, UserI user)
            throws WSFaultException, IOException {
        return getRecords(fact, conDet, user, String.format(GET_USER_QUERY, TABLE, user.getUserid()), TABLE, GET_FIELDS);
    }
}
