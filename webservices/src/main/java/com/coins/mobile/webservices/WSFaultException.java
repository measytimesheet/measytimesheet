package com.coins.mobile.webservices;

/**
 * Created by andrey on 18.06.2014.
 */
public class WSFaultException extends Exception {
    public WSFaultException(String message) {
        super(message);
    }
}
