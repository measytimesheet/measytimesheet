package com.coins.mobile.webservices;


import com.coins.mobile.webservices.buildxmlrequest.DbQuery;
import com.coins.mobile.webservices.buildxmlrequest.XMLRequestHelper;
import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.FactoryI;
import com.coins.mobile.webservices.interfaces.UserI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by andrey on 18.06.2014.
 */
public abstract class WebService<T> {

    protected List<T> getRecords
            (FactoryI<T> fact, ConnectionDetailsI conDet, UserI user, String strQuery, String table, String fields)
            throws WSFaultException, IOException {
        XMLRequestHelper<T> xmlRequestHelper = XMLRequestHelper.getInstance();

        DbQuery query = new DbQuery(strQuery, fields);
        String xml = xmlRequestHelper.RequestSOAPMessage(user, query);

        XMLRequestHelper.Response resp = xmlRequestHelper.sendXMLRequest(conDet, xml);

        List<T> empls = new ArrayList<T>();

        if (resp.getContentType().equalsIgnoreCase("text/xml")) {
            XMLRequestHelper.XMLResponseQuery XMLresp = xmlRequestHelper.parseXMLResponseRecords
                    (resp.getContent(), fact, table, Arrays.asList(fields.split(",")));

            if (XMLresp.getFault() != null)
                throw new WSFaultException(XMLresp.getFault().getReasonText());

            empls = XMLresp.getRecords();
        } else {
            throw new WSFaultException(resp.getContent());
        }
        return empls;
    }

}
