package com.coins.mobile.webservices.buildxmlrequest;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public abstract class Call {
    private final String object;
    private final String method;

    private final Map<String, String> params;

    protected Call(String object, String method) {
        this.object = object;
        this.method = method;
        params = new HashMap<String, String>();
    }

    protected void addParam(String name, String value) {
        params.put(name, value);
    }

    protected Set<Map.Entry<String, String>> getParams() {
        return params.entrySet();
    }

    protected String getObject() {
        return object;
    }

    protected String getMethod() {
        return method;
    }
}
