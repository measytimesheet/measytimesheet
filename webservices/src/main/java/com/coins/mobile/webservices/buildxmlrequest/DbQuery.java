package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public class DbQuery extends Call {

    public DbQuery(String pcQuery,String pcFields) {
        super("syudb", "dbquery");
        this.addParam("pcQuery",pcQuery);
        this.addParam("pcFields",pcFields);
    }
}
