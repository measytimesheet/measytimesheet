package com.coins.mobile.webservices.buildxmlrequest;

import android.util.Xml;

import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.FactoryI;
import com.coins.mobile.webservices.interfaces.UserI;
import com.coins.mobile.webservices.jcvjacWS;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public class XMLRequestHelper<T> {
    private final static String NAMESPACE = "http://www.w3.org/2003/05/soap-envelope";
    private static final String TAG = "XMLRequestHelper";

    private static XMLRequestHelper ourInstance = null;

    public static <V> XMLRequestHelper<V> getInstance() {
        if (ourInstance == null)
            ourInstance = new XMLRequestHelper<V>();

        return ourInstance;
    }

    private XMLRequestHelper() {
    }

    public String RequestSOAPMessage(UserI user, Call call) throws IOException {
        List<Call> calls = new ArrayList<Call>();
        calls.add(call);
        return RequestSOAPMessage(user, calls);
    }

    public String RequestSOAPMessage(UserI user, List<Call> calls) throws IOException {
        final XmlSerializer serializer;
        final StringWriter writer;

        serializer = Xml.newSerializer();
        writer = new StringWriter();

        serializer.setOutput(writer);
        serializer.startDocument("UTF-8", true);
        serializer.setPrefix("env", NAMESPACE);
        serializer.startTag(NAMESPACE, "Envelope");
        serializer.startTag(NAMESPACE, "Body");

        serializer.startTag("", "login");
        serializer.startTag("", "user");
        serializer.text(user.getUserid());
        serializer.endTag("", "user");
        serializer.startTag("", "password");
        serializer.text(user.getPassword());
        serializer.endTag("", "password");
        if (user.getKco() >= 0) {
            serializer.startTag("", "cid");
            serializer.text(String.valueOf(user.getKco()));
            serializer.endTag("", "cid");
        }
        serializer.endTag("", "login");

        if (calls != null) {
            for (Call call : calls) {

                if (call == null)
                    continue;

                final Set<Map.Entry<String, String>> params = call.getParams();

                serializer.startTag("", "call");
                serializer.attribute("", "object", call.getObject());
                serializer.attribute("", "method", call.getMethod());

                for (Map.Entry<String, String> param : params) {
                    serializer.attribute("", param.getKey(), param.getValue());
                }
                serializer.endTag("", "call");
            }
        }

        serializer.endTag(NAMESPACE, "Body");
        serializer.endTag(NAMESPACE, "Envelope");
        serializer.endDocument();
        return writer.toString();
    }

    public Response sendXMLRequest(ConnectionDetailsI conDet, String xml)
            throws IOException {
        HttpParams httpParameters = new BasicHttpParams();
        int timeoutConnection = 3000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);

        HttpClient httpclient = new DefaultHttpClient(httpParameters);
        HttpPost httppost = new HttpPost(conDet.getEnvironmentUrl());

        StringEntity httpEntity = new StringEntity(xml);

        httppost.addHeader("Content-Type", "text/xml");
        httppost.setEntity(httpEntity);

        HttpResponse response = httpclient.execute(httppost);

        return new Response(
                response.getFirstHeader("Content-Type").getValue(),
                EntityUtils.toString(response.getEntity()));
    }

    public XMLResponseQuery<T> parseXMLResponseRecords(String result, FactoryI<T> fact, String table, List<String> fieldsName) {

        List<T> res = new ArrayList<T>();
        XMLResponseQuery<T> XMLresp = new XMLResponseQuery<T>(res, null);
        Fault fault = null;
        try {
            XmlPullParserFactory parseFactory = XmlPullParserFactory.newInstance();

            parseFactory.setNamespaceAware(true);
            XmlPullParser xpp = parseFactory.newPullParser();

            Map<String, String> fields = null;

            xpp.setInput(new StringReader(result));
            int eventType = xpp.getEventType();
            String CurBodyChildTag = "";
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG &&
                        xpp.getDepth() == 3) {
                    CurBodyChildTag = xpp.getName();
                }

                if (eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("record") &&
                        xpp.getDepth() == 3) {
                    fields = new HashMap<String, String>();
                    fields.put(table + "rowid", xpp.getAttributeValue(0));
                } else if (eventType == XmlPullParser.END_TAG &&
                        xpp.getName().equalsIgnoreCase("record") &&
                        xpp.getDepth() == 3) {
                    res.add(fact.create(fields));
                    fields = null;
                } else if (eventType == XmlPullParser.START_TAG &&
                        fieldsName.contains(xpp.getName()) &&
                        xpp.getDepth() == 4 &&
                        CurBodyChildTag.equalsIgnoreCase("record")) {
                    String fieldName = xpp.getName();
                    try {
                        fields.put(fieldName, xpp.nextText());
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                } else if (eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("Fault") &&
                        xpp.getDepth() == 3) {
                    fault = new Fault();
                } else if (CurBodyChildTag.equalsIgnoreCase("Fault") &&
                        xpp.getDepth() == 5 &&
                        eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("Text")) {
                    try {
                        if (fault != null) fault.setReasonText(xpp.nextText());
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                } else if (CurBodyChildTag.equalsIgnoreCase("Fault") &&
                        xpp.getDepth() == 5 &&
                        eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("Code")) {
                    try {
                        if (fault != null) fault.setCodeValue(xpp.nextText());
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                }

                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        XMLresp = new XMLResponseQuery<T>(res, fault);

        return XMLresp;
    }

    public XMLResponse<T> parseXMLResponse(String result) {
        XMLResponse<T> XMLresp = new XMLResponse<T>(null);
        Fault fault = null;
        try {
            XmlPullParserFactory parseFactory = XmlPullParserFactory.newInstance();

            parseFactory.setNamespaceAware(true);
            XmlPullParser xpp = parseFactory.newPullParser();

            Map<String, String> fields = null;

            xpp.setInput(new StringReader(result));
            int eventType = xpp.getEventType();
            String CurBodyChildTag = "";
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG &&
                        xpp.getDepth() == 3) {
                    CurBodyChildTag = xpp.getName();
                }

                if (eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("Fault") &&
                        xpp.getDepth() == 3) {
                    fault = new Fault();
                } else if (CurBodyChildTag.equalsIgnoreCase("Fault") &&
                        xpp.getDepth() == 5 &&
                        eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("Text")) {
                    try {
                        if (fault != null) fault.setReasonText(xpp.nextText());
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                } else if (CurBodyChildTag.equalsIgnoreCase("Fault") &&
                        xpp.getDepth() == 5 &&
                        eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("Code")) {
                    try {
                        if (fault != null) fault.setCodeValue(xpp.nextText());
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                }

                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        XMLresp = new XMLResponse<T>(fault);

        return XMLresp;
    }

    public XMLResponseValidateCode parseXMLResponseValidateCode(String result) {
        jcvjacWS.ValidReturn validateReturn = new jcvjacWS.ValidReturn("", "", "");
        Fault fault = null;
        String validError = null;

        try {
            XmlPullParserFactory parseFactory = XmlPullParserFactory.newInstance();

            parseFactory.setNamespaceAware(true);
            XmlPullParser xpp = parseFactory.newPullParser();

            Map<String, String> fields = null;

            xpp.setInput(new StringReader(result));
            int eventType = xpp.getEventType();
            String CurBodyChildTag = "";
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG &&
                        xpp.getDepth() == 3) {
                    CurBodyChildTag = xpp.getName();
                }

                if (eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("call") &&
                        xpp.getAttributeValue("", "object").equalsIgnoreCase("jcvjac") &&
                        xpp.getAttributeValue("", "method").equalsIgnoreCase("getValidReturn") &&
                        xpp.getDepth() == 3) {
                    String pCode = xpp.getAttributeValue("", "pcCode");
                    String Value = "";
                    try {
                        xpp.nextTag();
                        Value = xpp.nextText();
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }

                    if (pCode.equalsIgnoreCase("jac_desc"))
                        validateReturn.setJac_desc(Value);
                    else if (pCode.equalsIgnoreCase("jac_entry"))
                        validateReturn.setJac_entry(Value);
                    else if (pCode.equalsIgnoreCase("jac_analysis"))
                        validateReturn.setJac_analysis(Value);
                } else if (eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("call") &&
                        xpp.getAttributeValue("", "object").equalsIgnoreCase("jcvjac") &&
                        xpp.getAttributeValue("", "method").equalsIgnoreCase("getValidError") &&
                        xpp.getDepth() == 3) {
                    try {
                        xpp.nextTag();
                        validError = xpp.nextText();
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                } else if (eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("Fault") &&
                        xpp.getDepth() == 3) {
                    fault = new Fault();
                } else if (CurBodyChildTag.equalsIgnoreCase("Fault") &&
                        xpp.getDepth() == 5 &&
                        eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("Text")) {
                    try {
                        if (fault != null) fault.setReasonText(xpp.nextText());
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                } else if (CurBodyChildTag.equalsIgnoreCase("Fault") &&
                        xpp.getDepth() == 5 &&
                        eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("Code")) {
                    try {
                        if (fault != null) fault.setCodeValue(xpp.nextText());
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    }
                }

                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        XMLResponseValidateCode XMLresp = new XMLResponseValidateCode(validateReturn, fault, validError);

        return XMLresp;
    }

    public static class Response {
        private String ContentType;
        private String Content;

        public Response(String contentType, String content) {
            ContentType = contentType;
            Content = content;
        }

        public String getContentType() {
            return ContentType;
        }

        public String getContent() {
            return Content;
        }
    }

    public static class XMLResponseQuery<T> {
        private List<T> records = new ArrayList<T>();
        private Fault fault;

        public XMLResponseQuery(List<T> records, Fault flt) {
            this.records = records;
            this.fault = flt;
        }

        public List<T> getRecords() {
            return records;
        }

        public Fault getFault() {
            return fault;
        }
    }

    public static class XMLResponseValidateCode {
        private Fault fault;
        private jcvjacWS.ValidReturn validateReturn;
        private String validError;

        public XMLResponseValidateCode(jcvjacWS.ValidReturn validateReturn, Fault flt, String validError) {
            this.validateReturn = validateReturn;
            this.fault = flt;
            this.validError = validError;
        }

        public jcvjacWS.ValidReturn getValidateReturn() {
            return validateReturn;
        }

        public Fault getFault() {
            return fault;
        }

        public String getValidError() {
            return validError;
        }
    }

    public static class XMLResponse<T> {
        private Fault fault;

        public XMLResponse(Fault flt) {
            this.fault = flt;
        }

        public Fault getFault() {
            return fault;
        }
    }

    public static class Fault {
        private String ReasonText;
        private String CodeValue;

        public Fault() {
            ReasonText = "";
            CodeValue = "";
        }

        public String getReasonText() {
            return ReasonText;
        }

        public String getCodeValue() {
            return CodeValue;
        }

        public void setReasonText(String ReasonText) {
            this.ReasonText = ReasonText;
        }

        public void setCodeValue(String CodeValue) {
            this.CodeValue = CodeValue;
        }
    }
}
