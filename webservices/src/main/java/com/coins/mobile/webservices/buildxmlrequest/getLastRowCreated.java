package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 17.06.2014.
 */
public class getLastRowCreated extends Call {
    public getLastRowCreated(String object) {
        super(object, "getLastRowCreated");
    }
}
