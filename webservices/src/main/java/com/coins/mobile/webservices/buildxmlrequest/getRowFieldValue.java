package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 17.06.2014.
 */
public class getRowFieldValue extends Call {
    public getRowFieldValue(String object, String pcField, int piIndex) {
        super(object, "getRowFieldValue");
        this.addParam("pcField", pcField);
        this.addParam("pbFormat", "No");
        this.addParam("piIndex", String.valueOf(piIndex));
    }
}
