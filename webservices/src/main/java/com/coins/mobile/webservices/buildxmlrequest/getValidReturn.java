package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 18.06.2014.
 */
public class getValidReturn extends Call {
    public getValidReturn(String object, String pcCode) {
        super(object, "getValidReturn");
        this.addParam("pcCode", pcCode);
    }
}