package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 17.06.2014.
 */
public class setBufferField extends Call {
    public setBufferField(String object,String pcField,int piExt, String pcValue) {
        super(object, "setBufferField");
        this.addParam("pcField",pcField);
        this.addParam("piExt",String.valueOf(piExt));
        this.addParam("pcValue",pcValue);
    }
}
