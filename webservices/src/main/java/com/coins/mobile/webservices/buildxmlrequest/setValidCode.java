package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 18.06.2014.
 */
public class setValidCode extends Call {
    public setValidCode(String object, String pcCode) {
        super(object, "setValidCode");
        this.addParam("pcCode", pcCode);
    }
}
