package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 17.06.2014.
 */
public class setVar extends Call {
    public setVar(String pcVar,String pcVal) {
        super("cos000", "setVar");
        this.addParam("pcVar",pcVar);
        this.addParam("pcVal",pcVal);
    }
}
