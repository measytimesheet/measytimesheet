package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 17.06.2014.
 */
public class setjc_timesheetQuery extends Call {
    public setjc_timesheetQuery(String pcQuery) {
        super("jts-rsp", "setjc_timesheetQuery");
        this.addParam("pcQuery", pcQuery);
    }
}
