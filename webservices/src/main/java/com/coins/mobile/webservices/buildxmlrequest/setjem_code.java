package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 18.06.2014.
 */
public class setjem_code extends Call {
    public setjem_code(String object, String pcCode) {
        super(object, "setjem_code");
        this.addParam("pcCode", pcCode);
    }
}
