package com.coins.mobile.webservices.buildxmlrequest;

/**
 * Created by Andrey.Zaburdaev on 18.06.2014.
 */
public class setjts_analysis extends Call {
    public setjts_analysis(String object, String pcAnal ) {
        super(object, "setjts_analysis");
        this.addParam("pcAnal", pcAnal);
    }
}