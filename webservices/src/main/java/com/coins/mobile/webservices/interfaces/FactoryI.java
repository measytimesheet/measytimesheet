package com.coins.mobile.webservices.interfaces;

import java.util.Map;

/**
 * Created by andrey on 17.06.2014.
 */
public interface FactoryI<T> {
    T create(Map<String, String> fields);
}
