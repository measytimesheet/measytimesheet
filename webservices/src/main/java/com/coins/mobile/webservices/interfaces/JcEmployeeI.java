package com.coins.mobile.webservices.interfaces;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public interface JcEmployeeI {
    public String getJem_name ();
    public String getJemRowid();
}
