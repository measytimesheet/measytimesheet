package com.coins.mobile.webservices.interfaces;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public interface JcTimesheetI {
    public long getDateMilliseconds();
    public String getActivity();
    public String getDescription();
    public String getAnalysis();
    public Double getHours();

}
