package com.coins.mobile.webservices.interfaces;

/**
 * Created by Andrey.Zaburdaev on 18.06.2014.
 */
public interface JcvJacValReturnI {
    public String getJac_desc();

    public String getJac_entry();

    public String getJac_analysis();
}
