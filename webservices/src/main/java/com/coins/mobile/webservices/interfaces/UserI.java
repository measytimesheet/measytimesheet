package com.coins.mobile.webservices.interfaces;

/**
 * Created by Andrey.Zaburdaev on 16.06.2014.
 */
public interface UserI {
    public String getUserid();
    public String getPassword();
    public int getKco();
}
