package com.coins.mobile.webservices;

import com.coins.mobile.webservices.buildxmlrequest.Call;
import com.coins.mobile.webservices.buildxmlrequest.XMLRequestHelper;
import com.coins.mobile.webservices.buildxmlrequest.getValidError;
import com.coins.mobile.webservices.buildxmlrequest.getValidReturn;
import com.coins.mobile.webservices.buildxmlrequest.isValid;
import com.coins.mobile.webservices.buildxmlrequest.setValidCode;
import com.coins.mobile.webservices.buildxmlrequest.setjem_code;
import com.coins.mobile.webservices.buildxmlrequest.setjts_analysis;
import com.coins.mobile.webservices.interfaces.ConnectionDetailsI;
import com.coins.mobile.webservices.interfaces.JcvJacValReturnI;
import com.coins.mobile.webservices.interfaces.UserI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey.Zaburdaev on 18.06.2014.
 */
public class jcvjacWS {
    private static final String TAG = "jcvjacWS";

    private static final String VALROG = "jcvjac";

    private static jcvjacWS ourInstance = new jcvjacWS();

    public static jcvjacWS getInstance() {
        return ourInstance;
    }

    private jcvjacWS() {
    }

    public ValidReturn validateCode(String validCode, String jemCode, ConnectionDetailsI conDet, UserI user) throws IOException, WSFaultException {
        List<Call> calls = new ArrayList<Call>();

        XMLRequestHelper<ValidReturn> xmlRequestHelper = XMLRequestHelper.getInstance();

        calls.add(new setValidCode(VALROG, validCode));
        calls.add(new setjem_code(VALROG, jemCode));
        calls.add(new setjts_analysis(VALROG, ""));
        calls.add(new isValid(VALROG));
        calls.add(new getValidReturn(VALROG, "jac_desc"));
        calls.add(new getValidReturn(VALROG, "jac_entry"));
        calls.add(new getValidReturn(VALROG, "jac_analysis"));
        calls.add(new getValidError(VALROG));

        String xml = xmlRequestHelper.RequestSOAPMessage(user, calls);

        XMLRequestHelper.Response resp = xmlRequestHelper.sendXMLRequest(conDet, xml);

        ValidReturn valRet = null;

        if (resp.getContentType().equalsIgnoreCase("text/xml")) {
            XMLRequestHelper.XMLResponseValidateCode XMLresp = xmlRequestHelper.parseXMLResponseValidateCode
                    (resp.getContent());

            if (XMLresp.getFault() != null)
                throw new WSFaultException(XMLresp.getFault().getReasonText());
            if (XMLresp.getValidError() != null && !XMLresp.getValidError().isEmpty())
                throw new WSFaultException(XMLresp.getValidError());

            valRet = XMLresp.getValidateReturn();

        } else {
            throw new WSFaultException(resp.getContent());
        }

        return valRet;
    }

    public static class ValidReturn implements JcvJacValReturnI {
        private String jac_desc;
        private String jac_entry;
        private String jac_analysis;

        public ValidReturn(String jac_desc, String jac_entry, String jac_analysis) {
            this.jac_desc = jac_desc;
            this.jac_entry = jac_entry;
            this.jac_analysis = jac_analysis;
        }

        public String getJac_desc() {
            return jac_desc;
        }

        public String getJac_entry() {
            return jac_entry;
        }

        public String getJac_analysis() {
            return jac_analysis;
        }

        public void setJac_desc(String jac_desc) {
            this.jac_desc = jac_desc;
        }

        public void setJac_entry(String jac_entry) {
            this.jac_entry = jac_entry;
        }

        public void setJac_analysis(String jac_analysis) {
            this.jac_analysis = jac_analysis;
        }
    }
}
